thisdir := $(dir $(lastword $(MAKEFILE_LIST)))
#===============================================================================

# This makefile fragment contains rules to use Kconfig for configuration.

#===============================================================================

# include guard
ifndef KCONFIG_MK
KCONFIG_MK = 1

include $(thisdir)help.mk
include $(thisdir)misc.mk

# toolchain.mk include so we can append _ccflags var
include $(thisdir)toolchain.mk

# the file where configuration should be stored
KCONFIG_CONFIG ?= $(outdir).config
#export KCONFIG_CONFIG

## the prefix to add to config symbols
#CONFIG_ ?= CONFIG_
#export CONFIG_

# the c header file to output configuration to
KCONFIG_AUTOHEADER ?= $(outdir)include/generated/autoconf.h
#export KCONFIG_AUTOHEADER

# 1 of 2 makefile fragment files to output configuration to
KCONFIG_AUTOCONFIG ?= $(outdir)include/config/auto.conf
#export KCONFIG_AUTOCONFIG

# 2 of 2 makefile fragment files to output configuration to
KCONFIG_TRISTATE ?= $(outdir)include/config/tristate.conf
#export KCONFIG_TRISTATE

# list of *_defconfig targets
KCONFIG_DEFCONFIGS ?= $(notdir $(wildcard config/*_defconfig))

# ==============================================================================

# add to help system
$(call help_add_target,"menuconfig","configure build settings")
$(foreach i, $(KCONFIG_DEFCONFIGS),\
 $(call help_add_target,"$i","use default build settings")\
)

# if they exist, include build configurations
-include $(KCONFIG_AUTOCONFIG)
-include $(KCONFIG_TRISTATE)

# tell the compiler to include header containing build configurations so that
# they may be used in the source code
KCONFIG_INCLUDES.gcc         = -include $(KCONFIG_AUTOHEADER)
KCONFIG_INCLUDES.codewarrior = -AddIncl$(KCONFIG_AUTOHEADER)
_ccflags += $(KCONFIG_INCLUDES.$(TOOLCHAIN))

.PHONY: menuconfig $(KCONFIG_DEFCONFIGS)

# rule to make .config from defaults, if KConfig exists
$(KCONFIG_CONFIG) : | KConfig
	mkdir -p $(dir $@)
	cd $(outdir)./; kconfig-conf --alldefconfig $(abspath $|)

# rule to generate includable c header files and makefile fragments containing
# build configurations
$(KCONFIG_AUTOHEADER) $(KCONFIG_AUTOCONFIG) &: KConfig $(KCONFIG_CONFIG)
	mkdir -p $(dir $(KCONFIG_AUTOHEADER) $(KCONFIG_AUTOCONFIG))
	cd $(outdir)./; kconfig-conf --silentoldconfig $(abspath $<)

# rule to interactively edit .config file
menuconfig : | KConfig
	mkdir -p $(dir $(KCONFIG_CONFIG))
	cd $(outdir)./; kconfig-mconf $(abspath $|)

# rule to make .config from a premade configuration in the config folder
$(KCONFIG_DEFCONFIGS) : %_defconfig : config/%_defconfig
	cd $(outdir)./; kconfig-conf --defconfig $(abspath $<)

# end include guard
endif

# vim: set noexpandtab :
