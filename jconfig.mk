#===============================================================================

# This makefile fragment contains rules to configure the build. It is generally
# more suitable than kconfig for very small projects with only a handful of
# configuration variables with little to no dependencies between them. It is
# also valid to use both jconfig and kconfig together.

#===============================================================================

# include guard
ifndef JCONFIG_MK
JCONFIG_MK = 1

# rule to create config.mk from template, only if config.mk does not exist yet
# and config_template.mk does
config.mk: | config_template.mk
	cp $| config.mk

# include site specific configurations (if present)
-include config.mk

# end include guard
endif

# vim: set noexpandtab :
