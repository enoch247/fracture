thisdir := $(dir $(lastword $(MAKEFILE_LIST)))
#===============================================================================

# This makefile fragment contains common rules and macros to build c++, c, and
# assembelly programs. Other toolchain specific fragments may include and extend
# it for their particular needs.

#===============================================================================

# include guard
ifndef TOOLCHAIN_MK
TOOLCHAIN_MK = 1

include $(thisdir)misc.mk

# the name of the toolchain to use for builds
# TOOLCHAIN =

# The toolchain programs to use to perform the build. These are defined such
# that each toolchain can define toolchain specific versions and we can switch
# between them simply by changing the value of the TOOLCHAIN macro. This allows
# a build to make use of multiple toolchains on a per directory basis. This can
# be handy, for exampe, if the application is to be cross compiled, but
# utilities or unit tests should still be built for the host machine.
#
# note: These cannot be overridden as env vars, and ?= does not work for many of
# them, because GNU Make already sets them as "implicit vars." All other
# override methods still work.
CXX     = $(or $($(TOOLCHAIN).CXX)     ,false)
CC      = $(or $($(TOOLCHAIN).CC)      ,false)
LD      = $(or $($(TOOLCHAIN).LD)      ,false)
AR      = $(or $($(TOOLCHAIN).AR)      ,false)
OBJCOPY = $(or $($(TOOLCHAIN).OBJCOPY) ,false)
DBUGR   = $(or $($(TOOLCHAIN).DBUGR)   ,false)

# macros for passing extra flags to toolchain programs
CPPFLAGS ?= $($(TOOLCHAIN).CPPFLAGS)
CXXFLAGS ?= $($(TOOLCHAIN).CXXFLAGS)
CFLAGS   ?= $($(TOOLCHAIN).CFLAGS)
LDFLAGS  ?= $($(TOOLCHAIN).LDFLAGS)

# collect program args from the commonly used variables for use by toolchains
_cppflags = $(strip $(cppflags) $(cppflags-y) $(CPPFLAGS) )
_cxxflags = $(strip $(cxxflags) $(cxxflags-y) $(CXXFLAGS) )
_ccflags  = $(strip $(ccflags)  $(ccflags-y)  $(CFLAGS)   )
_ldflags  = $(strip $(ldflags)  $(ldflags-y)  $(LDFLAGS)  )

# ==============================================================================

# rules to compile and link c++ files
$(outdir)%.o   : $(outdir)%.cpp       ; $(COMPILE.CXX)
$(outdir)%.o   : %.cpp                ; $(COMPILE.CXX)
$(outdir)%.elf : $(outdir)%.o | %.cpp ; $(LINK.CXX)
$(outdir)%     : $(outdir)%.o | %.cpp ; $(LINK.CXX)

# rules to compile and link c files
$(outdir)%.o   : $(outdir)%.c       ; $(COMPILE.C)
$(outdir)%.o   : %.c                ; $(COMPILE.C)
$(outdir)%.elf : $(outdir)%.o | %.c ; $(LINK.C)
$(outdir)%     : $(outdir)%.o | %.c ; $(LINK.C)

# rules to link object files, when none of the above patterns work
$(outdir)%.elf :              ; $(LINK)
$(outdir)%     : $(outdir)%.o ; $(LINK)
#$(outdir)%     :              ; $(LINK)

# rules to build static libraries
$(outdir)%.a:   ; $(ARCHIVE)
$(outdir)%.lib: ; $(ARCHIVE)

# rule to convert program from elf into other file formats
$(outdir)%.bin : $(outdir)%.elf ; $(ELF2BIN)
$(outdir)%.s19 : $(outdir)%.elf ; $(ELF2S19)

# ==============================================================================

# compile c++ file into object file
# and generate its accompanying .dep file
define COMPILE.CXX
	@echo MAKE: $@
	mkdir -p $(@D)
	$(or $($(TOOLCHAIN).COMPILE.CXX), false)
endef

# link c++ program
define LINK.CXX
	@echo MAKE: $@
	mkdir -p $(@D)
	$(or $($(TOOLCHAIN).LINK.CXX), false)
endef

# compile c file into object file
# and generate its accompanying .dep file
define COMPILE.C
	@echo MAKE: $@
	mkdir -p $(@D)
	$(or $($(TOOLCHAIN).COMPILE.C), false)
endef

# link c program
define LINK.C
	@echo MAKE: $@
	mkdir -p $(@D)
	$(or $($(TOOLCHAIN).LINK.C), false)
endef

# link unknown or mixed language program
define LINK
	@echo MAKE: $@
	mkdir -p $(@D)
	$(or $($(TOOLCHAIN).LINK), false)
endef

# archive files to make static libs
define ARCHIVE
	@echo MAKE: $@
	$(or $($(TOOLCHAIN).ARCHIVE), false)
endef

# generate .bin file from .elf file
define ELF2BIN
	@echo MAKE: $@
	mkdir -p $(@D)
	$(or $($(TOOLCHAIN).ELF2BIN), false)
endef

# generate .s19 file from .elf file
define ELF2S19
	@echo MAKE: $@
	mkdir -p $(@D)
	$(or $($(TOOLCHAIN).ELF2S19), false)
endef

# end include guard
endif

# vim: set noexpandtab :
