#===============================================================================

# This makefile fragment contains some things that are generally always helpful.

#===============================================================================

# include guard
ifndef MISC_MK
MISC_MK = 1

.PHONY: todolist

# the output directory for built files, as defined by the user
# This can be set at the command line with `make o=bin` or `make O=bin`. `O`
# takes precedence over `o` if both are set.
o ?=
O ?= $o

# the output directory for built files, cleaned up for use inside Makefiles
# if $O ends in a slash or is "/" , $(outdir) = $(O), else $(outdir) = $(O)/
outdir := $(if $(O),$(O)/)
outdir := $(patsubst %//,%/, $(outdir))

# disable builtin rules, we will supply our own
MAKEFLAGS += --no-builtin-rules

# rule to scan all files for todo comments and print them to the console
# note: using braces around "T" to avoid the grep being listed in the todolist
todolist:
	@grep --color=auto '[T]ODO' `find -type f`

# end include guard
endif

# vim: set noexpandtab :
