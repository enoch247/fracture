# About #

Fracture is a collection of useful Makefile fragments. Its goal is to reduce the amount of Makefile "boilerplate" needed when starting new software projects so that you can focus on writing code rather than your build system.

# Usage #

First you must copy the provided Makefile fragments to an accessible location. Probably the simplest way to do this, assuming you are already using git, is to add this repo as a git submodule to your project. Next, for each of fracture's modules of interest, add an include directive to your project's Makefile. For example:

``` Makefile
include PATH/TO/FRACTURE/gcc.mk
include PATH/TO/FRACTURE/kconfig.mk
include PATH/TO/FRACTURE/help.mk
```
Finally, start using the provided recipes, macros, etc provided by the included modules.

# Version Numbering #

Version numbers follow [Semantic versioning 2.0.0](https://semver.org/spec/v2.0.0.html).

