#===============================================================================

# This makefile fragment provides a `make help` target and macros that can be
# used to add to what is printed when `make help` is executed by a user.

#===============================================================================

# include guard
ifndef HELP_MK
HELP_MK = 1

.PHONY: help

# Targets section of the help message
HELP_TARGETS = "Targets:"

# macro to add a trget to the Targets section of the help message
# example usage: $(call help_add_target,"target","target description")
define help_add_target
$(eval HELP_TARGETS += "\n   $1 - $2")
endef

# add "help" target to help system
$(call help_add_target,"help","print this message")

# rule to print help
help ::
	@echo "Usage: make [target]"
	@echo
	@echo  $(HELP_TARGETS)

# end include guard
endif

# vim: set noexpandtab :
