thisdir := $(dir $(lastword $(MAKEFILE_LIST)))
#===============================================================================

# This makefile fragment contains rules to build c++, c, and assembelly programs
# using the toolchain supplied with CodeWarrior Classic outside of the IDE.

# example rules to build an application that uses the rules defined below:

# main.elf: ldflags += -lansixb
# main.elf:\
#  $(patsubst %.c, %.o, $(wildcard *.c))\
#  main.prm

# main.elf: ldflags += -lansixb
# main.elf:\
#  $(patsubst %.c, %.o, $(wildcard *.c))\
#  lib/codewarrior/hc12c/mc9s12xep100.lib\
#  mc9s12xep100.prm

# Note:
# The codewarrior compiler (and probably the assembeler) searches in the current
# working directory for files to satisfy include directives. It acts as if it
# was invoked with `-I .`. This is in contrast with many popular compilers, such
# as gcc, but is not in violation of the C standard.

#===============================================================================

# include guard
ifndef CODEWARRIOR_MK
CODEWARRIOR_MK = 1

include $(thisdir)toolchain.mk

# if no toolchain selected, set ourself as the default
TOOLCHAIN ?= codewarrior

# overrides when host machine is Linux
ifeq ($(shell uname -s), Linux)
CODEWARRIOR_ROOT ?= /opt/Freescale/CWS12v5.2
WINE ?= wine
endif

# DOS style 8.3 filenames for "Program Files" and "Program Files x86" to avoid
# spaces in names
Program_Files     ?= /c/PROGRA~1
Program_Files_x86 ?= /c/PROGRA~2

# the location were CodeWarrior is installed
# the default searchs several possible locations
CODEWARRIOR_ROOT ?= $(firstword $(wildcard\
  $(Program_Files_x86)/Freescale/CWS12v5.2\
  /c/CWS12v5.2\
 ))

# the location of CodeWarrior's toolchain programs
CODEWARRIOR_BINS ?= $(CODEWARRIOR_ROOT)/Prog

# the location where the libs shipped with CodeWarrior exist
CODEWARRIOR_LIBS ?= $(CODEWARRIOR_ROOT)/lib

# list of MCUs supported by the codewarrior toolchain
CODEWARRIOR_MCUS =\
 $(basename $(notdir $(wildcard $(CODEWARRIOR_LIBS)/hc12c/src/mc*.c)))

# ------------------------------------------------------------------------------

# Use CROSS_COMPILE to select the target architecture to build for. Valid values
# are:
#
# - hc12 - M68HC11, M68HC12, HCS12/MC9S12/MC9S12X, etc CPUs
# - xgate - the coprocessor found in MC9S12X MCUs
# - hc08 - reserved for future use to support 68HC08/68HCS08 CPUs
#
# For compatability with gcc style cross compiler prefixes, the follwing aliases
# are supported. (Note the trailing dash):
#
# - m68hc1x- - alias for hc12
# - m68hc11- - alias for hc12
# - m68hc12- - alias for hc12
#
CROSS_COMPILE ?= hc12

# CODEWARRIOR_CPU is equal to CROSS_COMPILE except with any aliases replaced
CODEWARRIOR_CPU = $(or $(CODEWARRIOR_CPU.$(CROSS_COMPILE)), $(CROSS_COMPILE))

# CROSS_COMPILE alias list
#CODEWARRIOR_CPU.alias-name = real
CODEWARRIOR_CPU.m68hc1x-    = hc12
CODEWARRIOR_CPU.m68hc11-    = hc12
CODEWARRIOR_CPU.m68hc12-    = hc12

# ------------------------------------------------------------------------------

# the toolchain binaries to use
codewarrior.AS    = $(WINE) $(CODEWARRIOR_BINS)/a$(CODEWARRIOR_CPU).exe
codewarrior.CC    = $(WINE) $(CODEWARRIOR_BINS)/c$(CODEWARRIOR_CPU).exe
codewarrior.LD    = $(WINE) $(CODEWARRIOR_BINS)/linker.exe
codewarrior.AR    = $(WINE) $(CODEWARRIOR_BINS)/libmaker.exe
codewarrior.DBUGR = $(WINE) $(CODEWARRIOR_BINS)/hiwave.exe

## add the compiler supplied linker scripts and c files to makes's search path so
## that they can be used as if they were in the current directory
#vpath %.prm $(CODEWARRIOR_LIBS)/$(CODEWARRIOR_CPU)c/prm
#vpath %.c   $(CODEWARRIOR_LIBS)/$(CODEWARRIOR_CPU)c/src
#vpath lib/% $(CODEWARRIOR_ROOT)

# additional build rules beyond what toolchain.mk provides
$(outdir)%.o   : %.S12                  ; $(codewarrior.ASSEMBLE.AS)
$(outdir)%.abs : $(outdir)%.o   | %.c   ; $(codewarrior.LINK.C)
$(outdir)%.abs : $(outdir)%.o   | %.S12 ; $(codewarrior.LINK.AS)
$(outdir)%.elf : $(outdir)%.o   | %.S12 ; $(codewarrior.LINK.AS)
$(outdir)%.abs:                         ; $(codewarrior.LINK)

# rule to compile library files from compiler vendor, storing the object file in
# the project folder
$(outdir)lib/codewarrior/%.o: $(CODEWARRIOR_LIBS)/%.c
	$(COMPILE.C)

# rule to create MCU specific libraries containing things like registers and
# startup code
$(foreach i, $(CODEWARRIOR_MCUS), $(outdir)lib/codewarrior/hc12c/$i.lib) :\
 $(outdir)lib/codewarrior/hc12c/%.lib :\
  $(outdir)lib/codewarrior/hc12c/src/%.o\
  $(outdir)lib/codewarrior/hc12c/src/start12.o\
  $(outdir)lib/codewarrior/hc12c/src/datapage.o\
  $(outdir)lib/codewarrior/hc12c/src/termio.o

# ==============================================================================
# implementation details:

# hide the GUI, don't create log files, and hush most console output
CODEWARRIOR_FLAGS =\
 -ViewHidden\
 -WErrFileOff\
 -WOutFileOff\
 -NoBeep\
 -W1\
 -WmsgNu=a\
 -WmsgNu=b\
 -WmsgNu=c\
 -WmsgNu=d\
 -WmsgNu=e\
 -WmsgNu=t

# add toolchain provided header files and libs to the toolchain's search paths
CODEWARRIOR_IDIRS ?= -I$(CODEWARRIOR_LIBS)/$(CODEWARRIOR_CPU)c/include
CODEWARRIOR_LDIRS ?= -L$(CODEWARRIOR_LIBS)/$(CODEWARRIOR_CPU)c/lib

# compile c file into object file
# and generate its accompanying .dep file
define codewarrior.COMPILE.C
	rm -f $@.dep
	$(CC)\
	 -objn=$(@D)/$(@F)\
	 -EnvTEXTPATH=$(@D)\
	 -EnvGENPATH=$(<D)\
	 -Lm=$@.dep -LmCfg=ilmox\
	 $(CODEWARRIOR_FLAGS) $(CODEWARRIOR_IDIRS) $(_ccflags)\
	 $<
	$(if $(WINE), sed -i "s|Z:`pwd`/||" $@.dep)
endef

# link c program
codewarrior.LINK.C = $(codewarrior.LINK)

# assemble assembly file into object file
# and generate its accompanying .dep file
define codewarrior.ASSEMBLE.AS
	rm -f $@.dep
	$(AS)\
	 -ObjN$(@D)/$(@F)\
	 -EnvOBJPATH=$(@D)\
	 -EnvTEXTPATH=$(@D)\
	 -EnvGENPATH=$(<D)\
	 -Lm=$@.dep -LmCfg=ilmox\
	 $(CODEWARRIOR_FLAGS) $(CODEWARRIOR_IDIRS) $(_asflags)\
	 $<
	$(if $(WINE), sed -i "s|Z:`pwd`/||" $@.dep)
	sed -i "s|$(basename $<).o|$@|" $@.dep
endef

# link assembly program
codewarrior.LINK.AS = $(codewarrior.LINK)

# link object files
define codewarrior.LINK
	$(LD)\
	 -O$@\
	 -EnvTEXTPATH=$(@D)\
	 $(CODEWARRIOR_FLAGS) $(CODEWARRIOR_LDIRS)\
	 $(filter-out -l%, $(_ldflags))\
	 $(filter %.prm, $^)\
	 $(addprefix -Add, $(filter %.o, $^) $(filter %.lib, $^))\
	 $(patsubst -l%, -Add%.lib, $(filter -l%, $(_ldflags)) $(LDLIBS))
endef

# archive files to make static libs
define codewarrior.ARCHIVE
	$(AR) $(CODEWARRIOR_FLAGS) "-Cmd("$^ = $@")"
endef

# FIXME: bailing out to gcc here, but this only works for hc12 and only when
# CROSS_COMPILE is set to a gcc style alias
include $(thisdir)gcc.mk
codewarrior.OBJCOPY = $(gcc.OBJCOPY)
codewarrior.ELF2BIN = $(gcc.ELF2BIN)
codewarrior.ELF2S19 = $(gcc.ELF2S19)

# end include guard
endif

# vim: set noexpandtab :
