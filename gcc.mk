thisdir := $(dir $(lastword $(MAKEFILE_LIST)))
#===============================================================================

# This makefile fragment contains rules to build c++, c, and assembelly programs
# using GCC.

# example rule to build an application that uses the rules defined below:

# main.elf:\
#  $(patsubst %.c, %.o, $(wildcard *.c))\

#===============================================================================

# include guard
ifndef GCC_MK
GCC_MK = 1

include $(thisdir)toolchain.mk

# if no toolchain selected, set ourself as the default
TOOLCHAIN ?= gcc

# cross compiler prefix
CROSS_COMPILE ?=

# the toolchain binaries to use
gcc.CXX     = $(CROSS_COMPILE)g++
gcc.CC      = $(CROSS_COMPILE)gcc
gcc.AR      = $(CROSS_COMPILE)ar
gcc.OBJCOPY = $(CROSS_COMPILE)objcopy
gcc.DBUGR   = $(CROSS_COMPILE)gdb

# set some sane default program flags
gcc.CXXFLAGS ?= -Wall -O3
gcc.CFLAGS   ?= -Wall -O3
gcc.LDFLAGS  ?=

# ==============================================================================
# implementation details:

# compile c++ file into object file
# and generate its accompanying .dep file
define gcc.COMPILE.CXX
	$(CXX) $(_cppflags) $(_cxxflags) -c -o $@ $<
	$(CXX) $(_cppflags) $(_cxxflags) -MM -MT $@ -MF $@.dep $<
endef

# link c++ program
define gcc.LINK.CXX
	$(CXX) $(_cxxflags) $(filter-out -l%, $(_ldflags)) -o $@\
	 $(filter %.o, $^) $(filter-out %.o, $^)\
	 $(filter -l%, $(_ldflags)) $(LDLIBS)
endef

# compile c file into object file
# and generate its accompanying .dep file
define gcc.COMPILE.C
	$(CC) $(_cppflags) $(_ccflags) -c -o $@ $<
	$(CC) $(_cppflags) $(_ccflags) -MM -MT $@ -MF $@.dep $<
endef

# link c program
define gcc.LINK.C
	$(CC) $(_ccflags) $(filter-out -l%, $(_ldflags)) -o $@\
	 $(filter %.o, $^) $(filter-out %.o, $^)\
	 $(filter -l%, $(_ldflags)) $(LDLIBS)
endef

# link object files, assume they are a c program
gcc.LINK = $(gcc.LINK.C)

# archive files to make static libs
define gcc.ARCHIVE
	$(AR) rcs $@ $?
endef

define gcc.ELF2BIN
	$(OBJCOPY) $< -O binary $@
endef

define gcc.ELF2S19
	$(OBJCOPY) $< -O srec $@
endef

# end include guard
endif

# vim: set noexpandtab :
